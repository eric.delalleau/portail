from alphabet import ALPHABETS

def vigenere_chiffre_message(msg, cle, alphabet = ALPHABETS['CAPITAL_LATIN']):
    """
    :param msg: (String) un message clair
    :param cle: (tuple of int) la clé de chiffrement
    :return: (String) le message chiffré

    :Exemples:

    >>> chiffre_vigenere("ABCDEFGHIJ", (1,2,3))
    'BDFEGIHJLK'
    """
    res = []
    i = 0
    for l in msg:
        res.append(alphabet[ (alphabet.index(l) + cle[i]) % len(alphabet) ])
        i = 0 if i == len(cle)-1 else i+1
    return "".join(res)


def vigenere_cle_inverse(cle, alphabet=ALPHABETS['CAPITAL_LATIN']):
    """
    :param cle: (tuple of int) une cle de chiffrement
    :return: (tuple of int) la cle de déchiffrement
    """
    return tuple( -k % len(alphabet) for k in cle )

def vigenere_dechiffre_message(msg, cle, alphabet = ALPHABETS['CAPITAL_LATIN']):
    """
    :param msg: (String) un message chiffré
    :param cle: (tuple of int) la clé de chiffrement
    :return: (String) le message clair

    :Exemples:

    >>> vigenere_dechiffre_message("BDFEGIHJLK", (1,2,3))
    'ABCDEFGHIJ'
    """
    return vigenere_chiffre_message(msg,
                                    vigenere_cle_inverse(cle, alphabet),
                                    alphabet)
