Création de machines virtuelles pour VirtualBox
=====================




Cette manipulation décrit comment créer une machine virtuelle pour VirtualBox,
dans laquelle on pourra installer un certain nombre d'applications.

Le déroulement est le suivant:
  - créer une image disque de base pour VirtualBox (`.vdi`) contenant la distribution
  choisie. On pourra conserver une image disque originelle de base exempte de
  tout logiciel supplémentaire.
  - cloner cette image originelle
  - installer les applications choisies dans l'image clonée
  - configurer la nouvelle machine virtuelle

Téléchargement de l'image iso d'une distribution
----
  - récupérer l'image iso de la distribution. Nous utilisons une ubuntu desktop 18.04.2 64 bits (1.9Go)
    (voir [downloads](https://www.ubuntu.com/download/desktop) ou [relesases](http://releases.ubuntu.com/18.04.2/)).
  - Après téléchargement, pensez à vérifier le sha256 () avec celui donné sur le site de téléchargement
  ```
  $ sha256sum /local/iso/...iso
  ```
  - il est aussi possible d'utiliser une image iso d'installation par réseau ([`Network installer`](https://ubuntu.com/download/alternative-downloads))
  qui téléchargera les paquets directement. Le choix des paquetages à installer est plus fin dans ce cas.

Création de l'image disque de base
---  
  - lancer virtualbox et créer une machine virtuelle
    - Nom: `ubuntubase`, Type: `Linux` Version: `Ubuntu (64-bit)`
    - Taille mémoire: `4096 Mo` (2048 Mo si vous n'avez que 4Go sur la machine hôte)
    - Disque dur: `Créer un disque dur virtuel maintenant`, taille `30,00 Gio`
    - Type de fichier de disque dur: `VDI (Image Disque VirtualBox)`
    - Stockage sur disque dur physique: `Dynamiquement alloué`
    - Emplacement du fichier et taille: laisser inchangés `ubuntubase` et `30,00Gio`
  - avant de lancer la machine virtuelle, mettez `presse-papier partagé` et `glisser/déposer` à `bidirectionnel` dans Général/Avancé
  - démarrer la machine virtuelle `ubuntubase` et utiliser l'iso comme image de démarrage quand VirtualBox vous le demande
  - on peut rajouter dans storage/IDE l'image iso qui sera alors disponible tout le temps et ensuite lancer la machine virtuelle
  - la suite dépend de la distribution choisie

Installation Ubuntu desktop
---
  - `Accueil`: choisir `install ubuntu`
  - `keyboard layout`: choisir `French` et `French`
  - `Updates and other software`: choisir `Minimal installation` et laisser `Download updates while installing Ubuntu`.
  - `Installation type`: laisser `Erase disk and install Ubuntu`
  - terminer l'installation en complétant les informations demandées (user/passwd: `localuser`/`localuser`)
  - redémarrer la machine virtuelle et connectez-vous pour vérifier son bon fonctionnement. Attendez également que le système
  ait fini sa mise à jour (processus `packagekitd/gnome-software`). Le `Software Updater` vous notifiera alors éventuellement
  des mises à jour à effectuer, effectuez-les maintenant et redémarrez à nouveau.
  - pour disposer du copier/coller, il faut installer les `VirtualBox Guest Additions`
  dans chaque machine virtuelle. Un fichier iso est disponible dans l'un de ces [répertoires](http://download.virtualbox.org/virtualbox/) selon la version de VirtualBox utilisée (voir `A propos de VirtualBox` dans le menu `Aide`). Lorsque la machine est démarrée, montez simplement l'iso à partir du menu `Périphériques`, `Lecteurs optiques`, et `Choisir l'image de disque`. L'iso est en éxécution automatique, confirmez l'exécution dans la fenêtre qui apparaîtra. Un redémarrage est nécessaire ensuite.

  - éteignez la machine virtuelle `ubuntubase`

Installation Ubuntu netinstall
---
  - L'image de boot à récupérer se nomme [mini.iso](http://archive.ubuntu.com/ubuntu/dists/bionic-updates/main/installer-amd64/current/images/netboot/mini.iso)
  - Utiliser les flèches pour sélectionner dans les menus texte, et <Tab> pour accéder aux 'boutons'
  - `Accueil`: choisir `Install`
  - `Language`/`Pays`: `French`
  - `Configurer le clavier`: `Non` (on choisit le layout)
  - `Disposition Clavier`: `French`/`French (legacy, alt...)`
  - après configuration automatique du réseau
  - `Nom machine`: `ubuntunet`
  - `Pays miroir de l'archive`: `France`/`fr.archive.ubuntu.com`
  - `Mandataire HTTP`: pas de mandataire dans mon cas (ou `cache-etu.univ-lille1.fr:3128` au M5?)
  - après récupération des paquetages de l'installer
  - `Nouvel utilisateur`: `localuser/localuser`
  - `Timezone`: confirmer `Europe/Paris` après récupération `ntp`
  - `Partitionnement disque`: `Assisté - utiliser un disque entier`
  - `Disque à parttionner`: `32.2 GB ATA VBOX HARDDISK` et `Appliquer les changements`: `Oui`
  - après récupération/installation du système de base
  - `Configuration de PAM`: `Pas de mises à jour automatiques`
  - `Choisir et installer des logiciels`: sélectionner `Lubuntu minimal installation`
    - vu passer `snapd` `llvm` ou `mono` par exemple, est-ce vraiment minimal ?
  - `Installer GRUB`: `Oui`
  - `Horloge système`: `à l'heure UTC`
  - et redémarrage (supprimer l'image `mini.iso` depuis le menu périphériques sinon recommencer ;-))




Clonage de la machine virtuelle de base
---
  - une fois notre image de base créée, nous clonons cette image afin de réaliser une machine virtuelle utilisable directement
  en séance de travaux pratiques: dans notre cas nous ajoutons un serveur web `apache2` et le logiciel `wireshark` de capture des paquets réseau.
  - le menu contextuel (bouton droit de la souris) permet de cloner la machine virtuelle `ubuntubase`:
    - le clone est nommé `ubuntureseaux2`
    - `Type de clone`: `clone intégral` (le fichier image `.vdi` est copié et non simplement lié)
    - la copie du fichier de 6.6Gio prend quelques minutes quand même (sur hdd)
  - une fois clonée lancer la machine virtuelle `ubuntureseaux2`

Installation des nouveaux logiciels et préparation de la machine clonée pour le TP
---
  - connectez vous à la machine virtuelle, lancez un terminal et passez `root` avec la commande `sudo -i`
  - installez `apache2` et `wireshark`
  ```
  apt-get update # mise à jour du cache
  apt-get install apache2 wireshark
  ```
  la configuration de `wireshark` demande si il faut autoriser les utilisateurs normaux à capturer des paquets; Laisser à non.
  - lancez `firefox` dans la machine virtuelle et saisissez `localhost` dans la barre d'url de `firefox`: la page d'accueil
  par défaut du serveur `apache2` apparaît, elle décrit comment configurer le serveur.   
  - nous remplacons ici simplement le fichier `var/www/html/index.html` par une page contenant un formulaire de login:
  ```
  mv /var/www/html/index.htm /var/www/html/index.html.orig
  cat > /var/www/html/index.html  
<html>
<head><meta charset="utf-8"/><title>Réseaux 2 -- Connexion</title></head>
<body>
  <form method="post">
  <fieldset>
  <legend>Connexion</legend>
  Utilisateur:<br>
  <input type="text" name="user"><br>
  Mot de passe:<br>
  <input type="password" name="passwd"><br><br>
  <input type="submit" value="Se connecter">
  </fieldset>
  </form>
</body>
</html>
  ```  
  - nous activons également le mode SSL d'`apache2` et le site par défaut correspondant
  ```
  a2enmod ssl
  a2ensite default-ssl.conf
  systemctl restart apache2
  ```

Principe de la gestion de paquetages (`apt-cache/apt-get`)
---
  - une distribution maintient un ensemble de paquetages disponibles dans ses dépôts
  - la gestion de paquetages assure la cohérence des dépendances entre ces paquetages
  - mise à jour du cache local de paquetages
  ```
  apt-get update
  ```
  - recherche d'un paquetage (application recherchée `nginx` ici) et informations
  ```
  apt-cache search nginx
  apt-cache show nginx
  ```
  - installation d'un paquetage (et de ses dépendances)
  ```
  apt-get install nginx
  ```
  - suppression d'un paquetage (et de ses dépendances)
  ```
  apt-get remove nginx
  ```


Notas
---
  - Aspect réseau de VirtualBox
    - [doc](https://www.virtualbox.org/manual/ch06.html)
    -  par défaut les invités ont une translation d'adresse réalisée par le service virtualbox lui-même (pas `iptables` sous linux): ils sont complétement isolés entre eux (et ont la même adresse ip) et injoignable depuis l'hôte, sauf à réaliser une redirection de port.
  - Taille de l'image vdi*
    - Ubuntu desktop: 7Gio
    - netinstall + Lubuntu Minimal: 4Gio (dont 1.5Gio de swapfile)
  - Pour installer avec succès les VGuestAdditions il faut installer dans les VMs gcc, make et perl...
